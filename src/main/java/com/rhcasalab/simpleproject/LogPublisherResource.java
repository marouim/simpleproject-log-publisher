package com.rhcasalab.simpleproject;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import javax.enterprise.context.ApplicationScoped;

import javax.inject.Inject;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.jboss.resteasy.annotations.SseElementType;



@ApplicationScoped
@Path("/logs")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class LogPublisherResource {

    @Inject @Channel("network-logs")
    Emitter<Log> emitter;

    @POST
    public Log sendLog(Log log) {
        
        emitter.send(log);
        return log;
    }
}